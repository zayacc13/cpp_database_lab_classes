﻿#include <iostream>
#include <stdio.h>
#include <fstream>
#include <Windows.h>
#include <stdio.h>
#pragma warning (disable:4996)
#define MaxSize 256
#define Len 110
#define CountComm 9
#define VoidSpace 10
#define MaxTypeSize 12
#define MaxFamSize 17
#define MaxWeightSize 12
#define MaxQuantitySize 15
#define MaxNumberSize 7
#define MaxCostSize 18



void FillSpace(int SpaceLen)
{
    for (int i = 0; i < SpaceLen; i++)
    {
        std::cout << ' ';
    }
}



int FindLenOfNumber(int Number)
{
    int count = 0;
    while (Number != 0)
    {
        Number = int(Number / 10);
        count += 1;
    }

    return count;
}



int NotCorrectType()
{
    int Number;
    int SpaceLen;
    while (!(std::cin >> Number) || (std::cin.peek() != '\n'))
    {
        std::cout << '\n';
        std::cin.clear();
        while (std::cin.get() != '\n');
        const char* text = "Ошибка ввода. Введите корректное значение";
        std::cout << text;
        SpaceLen = Len - strlen(text) - VoidSpace;
        FillSpace(SpaceLen);

    }

    return Number;
}



void FillStr()
{
    for (int i = 0; i < Len; i++)
    {
        std::cout << '-';
    }
    std::cout << '\n';
}



int NotCorrectNumber(int c, int a, int b)
{
    while (c < a || c > b)
    {
        c = NotCorrectType();
        while (c < a || c > b)
        {
            const char* text1 = "Введите корректное значение";
            std::cout << text1 << " ( от " << a << " до " << b << " )";
            FillSpace(Len - VoidSpace - strlen(text1) - 12 - FindLenOfNumber(a) - FindLenOfNumber(b));
            c = NotCorrectType();
        }
    }
    return c;
}



void SetDate(int& day, int& month, int& year)
{
    int SpaceLen;

    const char* t1 = "Введите день";
    std::cout << "Введите дату\n\n" << t1;

    SpaceLen = Len - strlen(t1) - VoidSpace;
    FillSpace(SpaceLen);

    day = NotCorrectType();
    std::cout << '\n';


    const char* t2 = "Введите месяц";
    std::cout << t2;
    SpaceLen = Len - strlen(t2) - VoidSpace;
    FillSpace(SpaceLen);

    month = NotCorrectType();
    std::cout << '\n';


    const char* t3 = "Введите год";
    std::cout << t3;
    SpaceLen = Len - strlen(t3) - VoidSpace;
    FillSpace(SpaceLen);

    year = NotCorrectType();
    std::cout << '\n';
}



class Date
{
    int day;
    int month;
    int year;

    friend class Food;

    friend std::pair<Food*, int> ReadStudentsData(std::ifstream& dataR, Food* Products);
};



class Food
{
    static int GlobalCount;

    char*       fam;
    int         type;
    double      weight;
    Date        date;
    
public:
    int         quant;
    double      cost;

    Food()
    {
        fam = (char*)malloc(MaxSize * sizeof(char));
        type = 0;
        date.day = 0;
        date.month = 0;
        date.year = 0;
        weight = 0;
        cost = 0;
        quant = 0;
    }

    Food(const char* Name, int t, int d, int mon, int y, int w, int c, int q)
    {
        fam = (char*)malloc((strlen(Name) + 1) * sizeof(char));
        strcpy(fam, Name);
        type = t;
        date.day = d;
        date.month = mon;
        date.year = y;
        weight = w;
        cost = c;
        quant = q;
    }

    Food(const Food& Element)
    {
        fam         = (char*)malloc((strlen(Element.fam) + 1) * sizeof(char));
        strcpy(fam, Element.fam);
        date.day    = Element.date.day;
        date.month  = Element.date.month;
        date.year   = Element.date.year;
        weight      = Element.weight;
        cost        = Element.cost;
        quant       = Element.quant;
        type        = Element.type;
    }

    void ProductInfo(int i)
    {
        std::cout << i;
        FillSpace(MaxNumberSize - FindLenOfNumber(i) - 1);
        std::cout << '|';

        std::cout << type;
        FillSpace(MaxTypeSize - FindLenOfNumber(type));
        std::cout << '|';

        std::cout << fam;
        FillSpace(MaxFamSize - strlen(fam));
        std::cout << '|';

        std::cout << quant;
        FillSpace(MaxQuantitySize - FindLenOfNumber(quant));
        std::cout << '|';

        std::cout << weight;
        FillSpace(MaxWeightSize - FindLenOfNumber(weight));
        std::cout << '|';

        std::cout << cost;
        FillSpace(MaxCostSize - FindLenOfNumber(cost));
        std::cout << "|";

        std::cout << date.day << '.' << date.month << '.' << date.year;
        int DateLen = FindLenOfNumber(date.day) + FindLenOfNumber(date.month) + FindLenOfNumber(date.year) + 2;
        FillSpace(Len - MaxWeightSize - MaxQuantitySize - MaxFamSize - MaxTypeSize - MaxCostSize - DateLen - 13);
        std::cout << "|\n";
    }


    void SetDay(int day) { date.day = day; }

    void SetMonth(int month) { date.month = month; }

    void SetYear(int year) { date.year = year; }

    int GetDay() { return date.day; }

    int GetMonth() { return date.month; }

    int GetYear() { return date.year; }


    int GetCount() { return GlobalCount; }

    void SetCount(int c) { GlobalCount = c; }

    friend  Food* NewProduct(Food* DataArray, std::ofstream& dataW);

    friend std::pair<Food*, int> ReadStudentsData(std::ifstream& dataR, Food* Products);

    friend void ClearMemory(Food* DataArray);

    friend void FindByFam(Food* Products);

    friend void SortByType(Food* Products);

    friend Food* CreateCopyOfElementByNumber(Food* DataArray, std::ofstream& dataW);


};



int IsValidCommandNumber(int c)
{
    while (c > CountComm || c < 1)
    {
        std::cout << "Вы ввели номер несуществующей команды.\n";
        std::cout << "Введите номер команды\t";
        std::cin >> c;
        std::cout << '\n';
    }
    return c;
}



int Interface()
{
    int command;
    int SpaceLen = 0;
    const char* Commands[] = { "Добавить информацию о новом товаре" ,
                                "Распечатать информацию о всех товарах в табличном виде",
                                "Поиск изделий по названию",
                                "Фильтр по типу" ,
                                "Сортировать по уменьшению стоимости" ,
                                "Создать копию выбранного элемента",
                                "Найти все продукты с данной датой выпуска",
                                "найти все изделия с датой изготовления больше заданной",
                                "Сохранить и выйти"};


    for (int i = 0; i < CountComm; i++)
    {
        std::cout << Commands[i];
        //4 - это 3 точки и пробел 
        SpaceLen = Len - strlen(Commands[i]) - 4 - FindLenOfNumber(i + 1);
        for (int j = 0; j < SpaceLen; j++)
        {
            std::cout << ' ';
        }
        std::cout << "... " << i + 1 << '\n';
    }


    command = NotCorrectType();
    std::cout << '\n';

    command = IsValidCommandNumber(command);

    return command;
}



void StartInterface()
{
    const char* NumberText      =   "Номер";
    const char* TypeText        =   "Тип изделия";
    const char* FamText         =   "Название изделия";
    const char* WeightText      =   "Вес изделия";
    const char* QuantityText    =   "Кол-во изделий";
    const char* CostText        =   "Стоимость изделия";
    const char* DateText        =   "Дата изготовления";
    int SpaceLen = 0;

    std::cout << NumberText;
    FillSpace(MaxNumberSize - strlen(NumberText) - 1);

    std::cout << '|' << TypeText;
    FillSpace(MaxTypeSize - strlen(TypeText));

    std::cout << '|' << FamText;
    FillSpace(MaxFamSize - strlen(FamText));

    std::cout << '|' << QuantityText;
    FillSpace(MaxQuantitySize - strlen(QuantityText));

    std::cout << '|' << WeightText;
    FillSpace(MaxWeightSize - strlen(WeightText));

    std::cout << '|' << CostText;
    FillSpace(MaxCostSize - strlen(CostText));

    std::cout << '|' << DateText;
    FillSpace(Len - MaxCostSize - MaxWeightSize - MaxQuantitySize - MaxFamSize - MaxTypeSize - MaxNumberSize - strlen(CostText) - 6);
    std::cout << "|\n";

    FillStr();
}



Food* NewProduct(Food* DataArray, std::ofstream& dataW)
{
    char* f = (char*)malloc(MaxSize * sizeof(char));
    int type, cost, quant, weight, day, month, year;
    int SpaceLen;
    DataArray->SetCount(DataArray->GetCount() + 1);
    int count = DataArray->GetCount();
    DataArray = (Food*)realloc(DataArray, count * sizeof(Food));



    const char* t1 = "Введите название товара";
    std::cout << t1;

    SpaceLen = Len - strlen(t1) - VoidSpace;
    FillSpace(SpaceLen);

    std::cin >> f;
    dataW << f << ' ';
    DataArray[count-1].fam = (char*)malloc((strlen(f) + 1) * sizeof(char));
    strcpy(DataArray[count-1].fam, f);
    std::cout << '\n';


    const char* t2 = "Введите тип изделия ( 1 - булочка; 2 - пирожок; 3 - пирожное )";
    std::cout << t2;

    SpaceLen = Len - strlen(t2) - VoidSpace;
    FillSpace(SpaceLen);

    type = NotCorrectNumber(5, 1, 3);
    DataArray[count-1].type = type;
    dataW << type << ' ';
    std::cout << '\n';


    const char* t3 = "Введите вес одного изделия ( г )";
    std::cout << t3;

    SpaceLen = Len - strlen(t3) - VoidSpace;
    FillSpace(SpaceLen);

    weight = NotCorrectType();
    DataArray[count-1].weight = weight;
    dataW << weight << ' ';
    std::cout << '\n';


    const char* t4 = "Введите кол-во изделий";
    std::cout << t4;

    SpaceLen = Len - strlen(t4) - VoidSpace;
    FillSpace(SpaceLen);

    quant = NotCorrectType();
    DataArray[count-1].quant = quant;
    dataW << quant << ' ';
    std::cout << '\n';


    const char* t5 = "Введите стоимость одного изделия ( руб )";
    std::cout << t5;

    SpaceLen = Len - strlen(t5) - VoidSpace;
    FillSpace(SpaceLen);

    cost = NotCorrectType();
    DataArray[count-1].cost = cost;
    dataW << cost << ' ';
    std::cout << '\n';


    SetDate(day, month, year);
    
    DataArray[count - 1].SetDay(day);
    dataW << day << ' ';

    DataArray[count - 1].SetMonth(month);
    dataW << month << ' ';

    DataArray[count - 1].SetYear(year);
    dataW << year << '\n';
    

    return DataArray;
}



void OutputProductsInfo(Food* Products)
{
    int count = Products->GetCount();
    if (count != 0)
    {
        StartInterface();
        for (int i = 0; i < count; i++)
        {
            Products[i].ProductInfo(i + 1);
        }
        FillStr();
    }
    else
    {
        std::cout << "На данный момент не было добавлено ни одного товара.\n";
    }
}



void FindByFam(Food* Products)
{
    int count = Products->GetCount();
    const char* text1 = "Введите название товара";
    std::cout << text1;
    int LocalCount = 0;
    FillSpace(Len - strlen(text1) - VoidSpace);
    Food* SpecificProducts;
    SpecificProducts = (Food*)malloc(sizeof(Food));

    char* buff = (char*)malloc(MaxSize * sizeof(char));
    std::cin >> buff;

    for (int i = 0; i < count; i++)
    {
        if (strcmp(Products[i].fam, buff) == 0)
        {
            LocalCount++;
            SpecificProducts[LocalCount - 1] = Products[i];
            SpecificProducts = (Food*)realloc(SpecificProducts, (LocalCount + 1) * sizeof(Food));
        }
    }
    if (LocalCount != 0)
    {
        StartInterface();
        for (int i = 0; i < LocalCount; i++)
        {
            SpecificProducts[i].ProductInfo(i + 1);
        }
        FillStr();
    }
    else
    {
        std::cout << "В базе пока нет товаров с таким названием\n\n";
    }

    free(buff);
    free(SpecificProducts);
}



void SortByType(Food* Products)
{
    int count = Products->GetCount();
    const char* text1 = "Введите тип товара";
    std::cout << text1;
    int LocalCount = 0;
    int Number;
    FillSpace(Len - strlen(text1) - VoidSpace);


    Number = NotCorrectNumber(5, 1, 3);


    Food* SpecificProducts;
    SpecificProducts = (Food*)malloc(sizeof(Food));


    for (int i = 0; i < count; i++)
    {
        if (Products[i].type == Number)
        {
            LocalCount++;
            SpecificProducts[LocalCount - 1] = Products[i];
            SpecificProducts = (Food*)realloc(SpecificProducts, (LocalCount + 1) * sizeof(Food));
        }
    }
    if (LocalCount != 0)
    {
        StartInterface();
        for (int i = 0; i < LocalCount; i++)
        {
            SpecificProducts[i].ProductInfo(i + 1);
        }
        FillStr();
    }
    else
    {
        std::cout << "В базе пока нет товаров такого типа\n\n";
    }

    free(SpecificProducts);
}



std::pair<Food*, int> ReadStudentsData(std::ifstream& dataR, Food* Products)
{
    int count = 0;
    int c2 = 0;
    int LocalCount = 0;
    int smth;


    while (!dataR.eof())
    {
        if (c2 < count)
        {
            Products = (Food*)realloc(Products, (count + 1) * sizeof(Food));
            c2++;
        }

        char* buff = (char*)malloc(MaxSize * sizeof(char));
        dataR >> buff;
        smth = LocalCount % 8;

        switch (smth)
        {
        case 0:
            Products[count].fam = (char*)malloc((strlen(buff) + 1) * sizeof(char));
            strcpy(Products[count].fam, buff);
            break;

        case 1:

            Products[count].type = std::atoi(buff);
            break;

        case 2:

            Products[count].weight = std::atoi(buff);
            break;

        case 3:

            Products[count].quant = std::atoi(buff);
            break;

        case 4:

            Products[count].cost = std::atoi(buff);
            break;

        case 5:

            Products[count].SetDay(std::atoi(buff));
            break;

        case 6:

            Products[count].SetMonth(std::atoi(buff));
            break;

        case 7:

            Products[count].SetYear(std::atoi(buff));
            break;
        }
        free(buff);

        LocalCount++;

        if (LocalCount >= 8 && LocalCount % 8 == 0) { count++; }
    }

    dataR.close();

    return std::pair<Food*, int>(Products, count);
}



void ClearMemory(Food* DataArray)
{
    int count = DataArray->GetCount();
    for (int i = 0; i < count; i++)
    {
        free(DataArray[i].fam);
    }

    free(DataArray);
}



void SortByCost(Food* Products)
{
    int count = Products->GetCount();
    Food buff;

    for (int i = 0; i < count; i++)
    {
        for (int j = 0; j < count - 1; j++)
        {
            if (Products[j].cost < Products[j + 1].cost)
            {
                buff = Products[j + 1];
                Products[j + 1] = Products[j];
                Products[j] = buff;
            }
        }
    }

    OutputProductsInfo(Products);
}



Food* CreateCopyOfElementByNumber(Food* DataArray, std::ofstream& dataW)
{
    int SpaceLen;
    int Number;

    const char* text = "Введите номер элемента";
    std::cout << text;
    SpaceLen = Len - strlen(text) - VoidSpace;
    FillSpace(SpaceLen);

    Number = NotCorrectNumber(0, 1, DataArray->GetCount());

    DataArray->SetCount(DataArray->GetCount() + 1);
    int count = DataArray->GetCount();
    DataArray = (Food*)realloc(DataArray, count * sizeof(Food));

    Food NewElement(DataArray[Number-1]);

    DataArray[count - 1].fam = (char*)malloc((strlen(NewElement.fam) + 1) * sizeof(char));
    strcpy(DataArray[count - 1].fam, NewElement.fam);
    DataArray[count - 1].type = NewElement.type;    DataArray[count - 1].weight = NewElement.weight;
    DataArray[count - 1].quant = NewElement.quant;  DataArray[count - 1].cost = NewElement.cost;
    DataArray[count - 1].SetDay(NewElement.GetDay());
    DataArray[count - 1].SetMonth(NewElement.GetMonth());
    DataArray[count - 1].SetYear(NewElement.GetYear());

    dataW << NewElement.fam  << ' ' << NewElement.type     << ' ' << NewElement.weight     << ' ' << NewElement.quant << ' ';
    dataW << NewElement.cost << ' ' << NewElement.GetDay() << ' ' << NewElement.GetMonth() << ' ' << NewElement.GetYear() << '\n';

    return DataArray;
}



void FindByDate(Food* DataArray)
{
    int count = DataArray->GetCount();
    int SpaceLen;
    int day, month, year;

    SetDate(day, month, year);

    int LocalCount = 0;
    Food* SpecificProducts;
    SpecificProducts = (Food*)malloc(sizeof(Food));


    for (int i = 0; i < count; i++)
    {
        if (DataArray[i].GetDay() == day && DataArray[i].GetMonth() == month && DataArray[i].GetYear() == year)
        {
            LocalCount++;
            SpecificProducts[LocalCount - 1] = DataArray[i];
            SpecificProducts = (Food*)realloc(SpecificProducts, (LocalCount + 1) * sizeof(Food));
        }
    }
    if (LocalCount != 0)
    {
        StartInterface();
        for (int i = 0; i < LocalCount; i++)
        {
            SpecificProducts[i].ProductInfo(i + 1);
        }
        FillStr();
    }
    else
    {
        std::cout << "В базе пока нет товаров с такой датой выпуска\n\n";
    }
    //ClearMemory(SpecificProducts);
    free(SpecificProducts);
}



void SortByDate(Food* DataArray)
{
    int day;
    int month; 
    int year;

    SetDate(day, month, year);
    
    int count = DataArray->GetCount();

    StartInterface();

    for (int i = 0; i < count; i++)
    {
        if (DataArray[i].GetYear() > year)
        {
            DataArray[i].ProductInfo(i + 1);
        }
        else if (DataArray[i].GetYear() == year)
        {
            if (DataArray[i].GetMonth() > month)
            {
                DataArray[i].ProductInfo(i + 1);
            }
            else if (DataArray[i].GetMonth() == month && DataArray[i].GetDay() >= day)
            {
                DataArray[i].ProductInfo(i + 1);
            }
        }
    }

    FillStr();
}







int Food::GlobalCount = 0;

int main()
{
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);

    Food* DataArray;
    DataArray = (Food*)malloc(1 * sizeof(Food));

    int command;
    bool flag = true;


    std::ofstream dataW;


    std::ifstream dataR;
    dataR.open("data2.txt");
    if (!dataR.is_open())
    {
        std::cout << "Ошибка открытия файла для чтения\n\n";
    }
    else
    {
        std::cout << "Файл для чтения открыт успешно\n\n";
    }

    std::pair<Food*, int> Smth = ReadStudentsData(dataR, DataArray);
    DataArray = std::get<0>(Smth);
    DataArray->SetCount(std::get<1>(Smth));


    while (flag)
    {
        command = Interface();

        switch (command)
        {
        case 1:

            dataW.open("data2.txt", std::ofstream::app);

            if (!dataW.is_open())
            {
                std::cout << "Ошибка открытия файла для записи\n\n";
            }
            else
            {
                std::cout << "Файл для записи открыт успешно\n\n";
            }

            DataArray = NewProduct(DataArray, dataW);
            dataW.close();

            break;

        case 2:

            OutputProductsInfo(DataArray);
            break;

        case 3:

            FindByFam(DataArray);
            break;

        case 4:

            SortByType(DataArray);
            break;

        case 5:

            SortByCost(DataArray);
            break;

        case 6:

            dataW.open("data2.txt", std::ofstream::app);

            if (!dataW.is_open())
            {
                std::cout << "Ошибка открытия файла для записи\n\n";
            }
            else
            {
                std::cout << "Файл для записи открыт успешно\n\n";
            }
            
            DataArray = CreateCopyOfElementByNumber(DataArray, dataW);

            dataW.close();

            break;

        case 7:

            FindByDate(DataArray);
            break;

        case 8:

            SortByDate(DataArray);
            break;

        case 9:

            ClearMemory(DataArray);

            flag = false;
            break;

        }
    }
}